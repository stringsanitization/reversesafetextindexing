/**
	RSDS: Reverse-safe Text Indexing
	Copyright (C) 2020 Grigorios Loukides, Solon P. Pissis, Huiping Chen
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

#define DEBUG false

#include <fstream>
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include <string.h>
#include <sstream>
#include <string>
#include <sys/time.h>
#include "rsds.h"
#include "dbg.h"

#include <time.h>
#include <stdlib.h>
#include <math.h>
#include <vector>

using namespace std;

int main( int argc, char ** argv )
{

  if(argc!=6)
  {
	cout<<"Run as ./rsds dataset.txt z zrcbp|zrc|zrcb|zrcbe k output_file\n";
	exit(-1);
  }  

  std::ifstream is(argv[1]);    

  std::string str(argv[2]);
  
  std::string str3(argv[3]);

  std::string str4(argv[4]);

  
  unsigned int k;             
  std::stringstream(str4)>>k;

  string output(argv[5]);

  bool dbl_prefix=false,no_gab=false,no_prefix=false,no_prefix_exp=false;

  
  if(str3.compare("zrcbp")==0)    //zrcbp  -- z-RCBP in the paper 
  {
        cout << "ZRCBP algorithm"<<endl;
        dbl_prefix=true; 
  }
  else if(str3.compare("zrc")==0)  //zrc   -- z-RC in the paper
  {
        cout << "ZRC algorithm"<<endl;
        dbl_prefix=false;    //no improvement II   
	no_prefix=true;      
        no_gab=true;         //does not use interval search
  }
  else if(str3.compare("zrce")==0) //zrce  -- z-RCE in the paper
  {
	cout << "ZRCE algorithm"<<endl;
	dbl_prefix=false;
	no_prefix=false;
	no_prefix_exp=true;   //exponential search
        no_gab=true;
  }
  else if(str3.compare("zrcb")==0)  //zrcb  -- z-RCB in the paper 
  {
        cout << "ZRCB algorithm"<<endl;
        dbl_prefix=false;
        no_prefix=true;    //does not use prefix
  } 
  else
  {      
	cout<<"Run as ./rsds dataset.txt z zrcbp|zrc|zrcb|zrce k output_file\n"; 
	 exit(-1);
  }

  unsigned int z;
	
  std::stringstream(str)>>z;
  if(z>=UINT_MAX)
  {
	cout<<"z should be smaller than: "<<UINT_MAX<<endl;
	exit(-1);  
  }
  else if(z<=1)
  {
	cout<<"z should be larger than 1."<<endl;
  }
  cout<<"z is "<<z<<endl;

vector<vector<char> > all_input_seqs;

  vector<char> input_seq_vec;
  char c;
  while (is.get(c))     
  {
      if(c=='\n')
      {
		break;
      }
      else
      {
        input_seq_vec.push_back(c);
      }
  }
  is.close();       


all_input_seqs.push_back(input_seq_vec);

  if(k>=input_seq_vec.size())
  {
        cout<<"k should be smaller than n: "<<input_seq_vec.size()<<endl;
        exit(-1);

  }
 // const char *seq = input_seq_vec.data();

   
  
  srand(time(NULL));   
  clock_t begin = clock();

   if(dbl_prefix)
  {
      INT line_cnt=0;
      for(auto &itt: all_input_seqs)
      {

         string tmpx(itt.begin(),itt.end());
         unsigned char * seq = (unsigned char*)tmpx.c_str();

	  
		int returned_d;
	 
		returned_d= binarydB_doubling_prefix((unsigned char*) seq,z, k);

	        cout<<"Returned: "<<returned_d<<endl;
		 if(returned_d>0)
		{
			string string_seq(reinterpret_cast<char*>(seq));

			euler_path(string_seq, returned_d, output, line_cnt);
			++line_cnt;
		 }
       }
  }
  if(no_prefix)
  {
	 INT line_cnt=0;
      for(auto &itt: all_input_seqs)
      {
         string tmpx(itt.begin(),itt.end());
         unsigned char * seq = (unsigned char*)tmpx.c_str();

         int returned_d;
         if(no_gab)
         {
		
	        returned_d= binarydB_no_prefix((unsigned char*) seq,z, k);
		}
	 else
	 {
			
			returned_d = binarydB_no_prefix_gab((unsigned char*) seq,z, k);
         }
        
        cout<<"Returned: "<<returned_d<<endl;
        if(returned_d>0)
        {
            string string_seq(reinterpret_cast<char*>(seq));

            euler_path(string_seq, returned_d, output, line_cnt);
            ++line_cnt;
         }

       }
	}
   if(no_prefix_exp)
  {
         INT line_cnt=0;
      for(auto &itt: all_input_seqs)
      {
         string tmpx(itt.begin(),itt.end());
         unsigned char * seq = (unsigned char*)tmpx.c_str();
         int returned_d;

         
         returned_d= binarydB_no_prefix_exp((unsigned char*) seq,z, k);

         cout<<"Returned: "<<returned_d<<endl;
         if(returned_d>0)
        {
            string string_seq(reinterpret_cast<char*>(seq));
         
            euler_path(string_seq, returned_d, output, line_cnt);
            ++line_cnt;
         }
       }

  }

  clock_t end = clock();

  double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
  printf("Time needed %lf secs\n", elapsed_secs);


     
	return ( 0 );
}
